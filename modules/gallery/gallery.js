var $gallery = $('.gallery').fotorama({
	width: '100%',
	nav: 'thumbs',
	thumbwidth: 170,
	thumbheight: 100,
	thumbmargin: 30
});