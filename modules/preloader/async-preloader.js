(function () {
	var preloader = new Object({
		element: document.querySelector('.preloader'),
		className: '_active',
		show: function () {
			this.element.classList.add(this.className)
		},
		hide: function () {
			this.element.classList.remove(this.className)
		},
		init: function () {
			this.show();
			document.addEventListener('DOMContentLoaded', function () {
				preloader.hide();
			});
		}
	});
	preloader.init();
})();