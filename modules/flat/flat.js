;NodeList.prototype.forEach = Array.prototype.forEach;
;(function () {
	var tabs = new Object({
		active: 0,
		className: '_active',
		links: document.querySelectorAll('.flat__menu__link'),
		content: document.querySelectorAll('.flat__info-tab'),
		change: function (numb) {
			this.toggle('remove');
			this.active = numb;
			this.toggle('add');
		},
		toggle: function (method) {
			this.links[this.active].classList[method](this.className);
			this.content[this.active].classList[method](this.className);
		},
		init: function () {
			this.links.forEach(function (item, index) {
				item.addEventListener('click', function (e) {
					tabs.change(index);
					e.preventDefault();
				})
			});
		}
	});
	tabs.init();
})();