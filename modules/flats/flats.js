;NodeList.prototype.forEach = Array.prototype.forEach;
;(function () {
	var flats = document.querySelectorAll('.svg-flat');
	var flat = document.querySelector('.flats__flat');
	var back = document.querySelector('.flat__back');
	
	back.addEventListener('click', function () {
		flat.classList.remove('_open');
	});
	
	flats.forEach(function (item) {
		item.addEventListener('click', function () {
			flat.classList.add('_open');
		});
	});
	
	var nav = new Object({
		active: 0,
		className: '_active',
		links: document.querySelectorAll('.svg-section'),
		labels: document.querySelectorAll('.flats__sections__label'),
		sections: document.querySelectorAll('.flats__section'),
		init: function () {
			this.links.forEach(function (item, index) {
				item.addEventListener('click', function () {
					var numb = this.getAttribute('data-section');
					nav.toggle(numb - 1);
				});
			});
		},
		toggle: function (numb) {;
			this.changeClass(this.active, 'remove');
			this.active = numb;
			this.changeClass(this.active, 'add');
		},
		changeClass: function (index, method) {
			var classList = this.links[index].getAttribute('class');
			if (method === 'add') {
				classList += ' ' + this.className;
			}
			if (method === 'remove') {
				classList = classList.replace(' ' + this.className, '');
			}
			this.links[index].setAttribute('class', classList);
			this.labels[index].classList[method](this.className);
			this.sections[index].classList[method](this.className)
		}
	});
	nav.init();
})();