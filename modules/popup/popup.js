(function () {
	var popup = new Object({
			activeClass: '_open',
			title: 'Записаться на просмотрт',
			open: function (popupName) {
				var popup = document.querySelector('[data-popup="' + popupName + '"]');
				popup.classList.add(this.activeClass);
			},
			openByHash: function () {
				if (location.hash != '' && location.hash.indexOf('#popup/') >= 0) {
					this.open(location.hash.replace('#popup/', ''));
				}
			},
			close: function () {
				$('.popup').removeClass(this.activeClass);
				history.back();
			},
			init: function () {
				window.addEventListener('load', function (e) {
					popup.openByHash();
				});
				window.addEventListener('hashchange', function (e) {
					popup.openByHash();
				});
				
				$('[href*="#popup/"]').click(function () {
					var title = $(this).attr('data-title') || popup.title;
					var name = $(this).attr('href').replace('#popup/', '')
					
					$('[data-popup="' + name + '"]').find('.form__title').text(title);
				});
				
				$('.popup__close,.popup__overlay').click(function () {
					popup.close();
				});
			}
		});
	popup.init();
})();