(function () {
	ymaps.ready(init);
	var map_house,
		placemark_house,
		coords_house = [57.15355002252341, 65.52313049734497],
		map_office,
		placemark_office,
		coords_office = [57.155635771331646,65.54722749999993],
		placemark_settigns = {
			iconLayout: 'default#image',
			iconImageHref: '/img/placemark.png',
			iconImageSize: [23, 30],
			iconImageOffset: [-11, -30]
		};
	
	function init() {
		// house
		map_house = new ymaps.Map('map_house', {
			center: coords_house,
			zoom: 17,
			controls: [],
			behaviors: []
		});
		
		placemark_house = new ymaps.Placemark(coords_house, {
			balloonContent: 'Дом!'
		}, placemark_settigns);
		
		map_house.geoObjects.add(placemark_house);
		
		// office
		map_office = new ymaps.Map('map_office', {
			center: coords_office,
			zoom: 17,
			controls: [],
			behaviors: []
		});
		
		placemark_office = new ymaps.Placemark(coords_office, {
			balloonContent: 'Офис!'
		}, placemark_settigns);
		
		map_office.geoObjects.add(placemark_office);
	}
})();